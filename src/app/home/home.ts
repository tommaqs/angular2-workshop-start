import {Component, OnInit} from 'angular2/core';

@Component({
  selector: 'home',
  providers: [],
  styles: [require('./home.css')],
  template: require('./home.html')
})
export class Home implements OnInit {
  constructor() {
  }

  public title:String = 'Homepage';

  ngOnInit() {
    console.log('hello ng-Mates!');
  }
}
