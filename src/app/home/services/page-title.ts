import {Injectable} from 'angular2/core';

@Injectable()
export class PageTitle {
  constructor() {
  }

  getTitle() {
    return 'ngWorkshop';
  }
}
