import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {FORM_PROVIDERS} from 'angular2/common';

import {Home} from './home/home';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  providers: [ ...FORM_PROVIDERS ],
  directives: [ ...ROUTER_DIRECTIVES],
  pipes: [],
  styles: [`
    .splash {
      padding-top: 50px;
    }
  `],
  template: `
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header hidden-md-up">
          <button type="button" class="navbar-toggle navbar-toggler pull-right">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Grants</a>
        </div>
        <div class="hidden-xs hidden-xs-down">
          <ul class="nav navbar-nav">
            <li class="nav-item"><a class="nav-link">Item #1</a></li>
            <li class="nav-item"><a class="nav-link">Item #2</a></li>
            <li class="nav-item"><a class="nav-link">Item #3</a></li>
          </ul>
        </div>
        <div class="visible-xs hidden-md-up">
          <ul class="nav nav-pills nav-stacked scrollable-menu">
            <li class="nav-item"><a class="nav-link">Item #1</a></li>
            <li class="nav-item"><a class="nav-link">Item #2</a></li>
            <li class="nav-item"><a class="nav-link">Item #3</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="splash"></div>

    <main>
      <router-outlet></router-outlet>
    </main>

    <div class="footer">
      <div class="container text-center">
        Angular 2 Workshop 2016.02.27
      </div>
    </div>
  `
})
@RouteConfig([
  { path: '/', component: Home, name: 'Index' },
  { path: '/home', component: Home, name: 'Home' },
  { path: '/**', redirectTo: ['Index'] }
])
export class App {}
